import React, { useState, useEffect } from 'react';
import './book.sass'
import {  Row, Col } from 'react-bootstrap';
const Book=(props)=>{
    return (
        <Col  lg={4}   className="card">
            <Col lg={12} className="information">
               <Col lg={4} className="noPaddingLeft">
                    <img src={(props.book.volumeInfo.imageLinks!=undefined)?props.book.volumeInfo.imageLinks.thumbnail:`/images/noimage.png`} className="bookImg"/>
               </Col>
               <Col lg={8}>
               <p className="title">
                        <b>{(props.book.volumeInfo.title)?props.book.volumeInfo.title:''}</b>
                        <span className="author">By {(props.book.volumeInfo.authors)?props.book.volumeInfo.authors:''}</span>
                    </p>
                    <p className="bookDescription">
                        {(props.book.volumeInfo.description)?props.book.volumeInfo.description:''}
                    </p>
               </Col>
            </Col>
        </Col> 
    )
    
}
export default Book;