import React from 'react';
import  './menu.sass';
import { Nav, Navbar, Col, Row, Container } from 'react-bootstrap';
const Menu =()=>{
    return (
            <Container >
                <Navbar expand="lg" className="nav" >
                    <Navbar.Brand href="#home" className="titlePage">Kvothe´s Books</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"  />
                    <Navbar.Collapse id="basic-navbar-nav" className="opcOptions">
                        <Nav>
                            <Nav.Link href="#News" className="links">News</Nav.Link>
                            <Nav.Link href="#books" className="links">Books</Nav.Link>
                            <Nav.Link href="#events" className="links">Events</Nav.Link>
                            <Nav.Link href="#about" className="links">About</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </Container>
    )
}

export default Menu;