import React, { useState, useEffect } from 'react';
import LoadingBooks from './../loadingbooks';
import   './inputbook.sass';
import {SETBOOKS,GETBOOKS,SETBOOKSA,GETBOOKSA} from '../../redux/actions/booksActions';
import { Form,FormControl,Button,Row,Col  } from 'react-bootstrap';
import {connect} from 'react-redux';
import axios from 'axios';

const InputBook=(props)=>{
    const ApiKey='AIzaSyCh7JT3r5wmotFv7TX_sZ_tlPcOdng8Lko';
    const limitRows=12;
    const urlApi='https://www.googleapis.com/books/v1/volumes';
    const [book,setBook]= useState('');
    const [isFetching,setFetching]= useState(false);

    useEffect(()=>{
        if(book!=""){
            setFetching(true);
            axios.get(`${urlApi}?q=${book}&maxResults=${limitRows}&key=${ApiKey}`).then(res=>{
                setTimeout(()=>{
                    setFetching(false);
                    props.SETBOOKS({data:res.data.items,isFetching:isFetching});  
                },300)
                
            });
            return ()=>{}
        }
            
    },[book]);
    const handleSubmit=(e)=>{
        e.preventDefault();
        
        setBook(e.target[0].value);
    }
    if(isFetching){
        return (
            <React.Fragment>
                <Col lg={12} className="serachContainer">
                    <Form inline onSubmit={handleSubmit} >
                        <FormControl type="text" placeholder="Find a book" className="inputSearch" />
                        <Button variant="outline-success" type="submit" className="buttonInput">Search</Button>
                    </Form>
                </Col>
                <Col lg={12} >&nbsp;</Col>
                
                    <LoadingBooks/>
                    <LoadingBooks/>
                    <LoadingBooks/>
                    <LoadingBooks/>
                    <LoadingBooks/>
                    <LoadingBooks/>
                    <LoadingBooks/>
                    <LoadingBooks/>
                
            </React.Fragment>
        )
    }else
    return (
        
        <Col lg={12} className="serachContainer">
            <Form inline onSubmit={handleSubmit} >
                <FormControl type="text" placeholder="Find a book" className="inputSearch" />
                <Button variant="outline-success" type="submit" className="buttonInput" >Search</Button>
            </Form>
       </Col> 
       
        
        
        
    )
   
}
const mapStateToProps=(state)=>{
    return {
        ...state,
        books:state.books,

    }
}
const mapDispatchToPorps=(dispatch)=>{
    return {
        SETBOOKS:(data)=>{dispatch(SETBOOKSA(data))},
        GETBOOKS:dispatch(GETBOOKSA())
    }
}

export default connect(mapStateToProps,mapDispatchToPorps)(InputBook);

