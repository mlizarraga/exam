import React from 'react';
import   './homeimage.sass';
const HomeImage=(props)=>{  
    return (
        <picture className="imgHome">
            <source media="(min-width:300px) and (max-width: 700px)" srcset={process.env.PUBLIC_URL + props.urlImgSmall} />
            <source media="(min-width: 651px) and (max-width:1300px)" srcset={process.env.PUBLIC_URL + props.urlImg} />
            <img src={process.env.PUBLIC_URL + props.urlImg} />
        </picture>
        
    )
}
export default HomeImage;