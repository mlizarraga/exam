import React from 'react';
import {connect} from 'react-redux';
import {  Row, Col } from 'react-bootstrap';
import  '../book/book.module.css';

const LoadingBooks=(props)=>{
    
    return(
             <Col  lg={4} className="card">
                <Col lg={12} className="information">
                <Col lg={4} className="noPaddingLeft" style={{
                    backGroundColor:'grey'
                }}>
                </Col>
                <Col lg={8}>
                <p className="title">
                            <b>..........</b>
                            <span className="author">By ..........</span>
                        </p>
                        <p className="bookDescription">
                            ..............<br/>
                            ..................
                        </p>
                </Col>
                </Col>
            </Col>)
}

const mapStateToProps=(state)=>{
    return {
        isFetching:state.isFetching
    }
}
export default connect(mapStateToProps)(LoadingBooks);