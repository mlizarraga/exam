import React from 'react';
import Book from '../book';
import {connect} from 'react-redux';
const ContainerBooks=(props)=>{
    let numCharacters=60,
    numCharactersTitle=35,
    authorCharacter=20;
    if (window.screen.width >= 250 &&  window.screen.width<360){
        numCharacters=45;
        numCharactersTitle=10;
        authorCharacter=7;
    }else if (window.screen.width <= 360) {
        numCharacters=55;
        numCharactersTitle=15;
        authorCharacter=10;
    }else if (window.screen.width <= 450) {
        numCharacters=90;
        numCharactersTitle=20;
    }
        
    console.log(props.books);
    if(props.books!=undefined){
        
            
        return props.books.map((book,index)=>{
            if(book.volumeInfo.description!=undefined){
                if(book.volumeInfo.description!=undefined){
                    if(+book.volumeInfo.description.length>numCharacters)
                        book.volumeInfo.description=book.volumeInfo.description.slice(0,numCharacters)+'...';
                }
            }
            if(book.volumeInfo.title!=undefined){
                if(book.volumeInfo.title.length>numCharactersTitle)
                    book.volumeInfo.title=book.volumeInfo.title.slice(0,numCharactersTitle)+'...';
            }
            if(book.volumeInfo.authors!=undefined){
                if(book.volumeInfo.authors.length>1)
                    book.volumeInfo.authors=book.volumeInfo.authors[0].slice(0,authorCharacter)+'...';
                else
                    book.volumeInfo.authors=book.volumeInfo.authors[0].slice(0,authorCharacter)+'...';
            }

            return (<Book book={book} key={index}/>)
        })
    }
    else
        return (<div className="empty">&nbsp;</div>)
}

const mapStateToProps=(state)=>{
    return {
        books:state.books
    }
}
export default connect(mapStateToProps)(ContainerBooks);
