import React from 'react';
import './messagehome.sass';
import {Col, Button} from 'react-bootstrap';
const MessageHome =()=>{
    return (
        <Col lg={5}>
            <div className="messageHome">
                <div className="message">
                    <h2>You haven´t read anything yet</h2>
                </div>
                <div className="subMessage">
                    <p>They say outside of a dog, a book is man´s best friend (inside of a dog it´s too dark to read). Come in,chat with us, share your favorite books & films and we´ll introduce you to some of the greatest frinds you´ll ever know</p>
                </div>
                <div className="footerMessage">
                    <Button variant="success" className="enroll">Come visit us!</Button>
                </div>
            </div>    
        </Col>
        
    )
}
export default MessageHome;