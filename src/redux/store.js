import {createStore} from 'redux';
import setBooks from '../redux/reducers/booksReducer';

const store=createStore(setBooks);
export default store;