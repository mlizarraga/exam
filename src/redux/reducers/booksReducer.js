import {SETBOOKS,GETBOOKS} from '../actions/booksActions';
const initialState=[];

export default function setBooks(state=[],actions){
    switch(actions.type){
        case SETBOOKS:
            return {books:actions.payload.books.data,isFetching:actions.payload.books.isFetching}
        break;
        case GETBOOKS:
            return {books:state.data,isFetching:false}
        break;
        default:
            return {books:state,isFetching:false}
    }
}