import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './components/menu';
import InputBook from './components/inputbook'
import HomeImage from  './components/homeimage';
import ContainerBooks from './components/containerbooks';
import LoadingBooks from './components/loadingbooks';
import MessageHome from './components/messagehome';
import store from './redux/store';
import{Provider} from 'react-redux'
import { Container, Row, Col } from 'react-bootstrap';

function App() {
  return (
    <Provider
      store={store}
    >
        <Container fluid>
          <Row className="headeer">
            <header className="nav fixed-top">
              <Menu></Menu>
            </header>
            <Container>
              <MessageHome></MessageHome>
            </Container>
            <HomeImage
              urlImg="/images/homeImage.jpg"
              urlImgSmall="/images/small.png"
            />
          </Row>
          <Row className="body">
            <Container >
              <Row>
                    <InputBook />
                <Col lg={12 }>&nbsp;</Col>
              </Row>
              <Row>
                  <ContainerBooks/>
              </Row>
            </Container>
          </Row>
          <Row className="footer">
            <Container className="noPadding">
                <footer>2012 Created by [Manuel Lizarraga]</footer>
            </Container>
          </Row>
      </Container>
    </Provider>
    
  );
}
export default App;
